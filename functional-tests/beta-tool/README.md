## Mock for Beta Tool
Use this docker-compose to verify locally, that integration with beta tool (tracking) is working properly.
Run the tracking mock server with command:

    ``
    docker-compose -f .\functional-tests\beta-tool\tracking-mock-docker-compose.yml up
    ``
    
This will create beta-tool_default network, and use this network to run membership like this:

    ``
    docker run -i -t -p 8080:8080 -p 8787:8787 -v /c/dev/logsDocker:/opt/jboss/jboss-eap-6.4/standalone/log --network beta-tool_default membership
    ``
  