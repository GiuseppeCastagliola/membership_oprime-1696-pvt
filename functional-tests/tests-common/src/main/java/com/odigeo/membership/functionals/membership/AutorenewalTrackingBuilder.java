package com.odigeo.membership.functionals.membership;

import java.time.Instant;
import java.util.UUID;

public class AutorenewalTrackingBuilder {
    private String membershipId;
    private Instant timestamp;
    private String autoRenewalOperation;
    private String requestedMethod;
    private String requester;
    private int interfaceId;
    private UUID id;

    public AutorenewalTrackingBuilder setMembershipId(String membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public AutorenewalTrackingBuilder setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public AutorenewalTrackingBuilder setAutoRenewalOperation(String autoRenewalOperation) {
        this.autoRenewalOperation = autoRenewalOperation;
        return this;
    }

    public AutorenewalTrackingBuilder setRequestedMethod(String requestedMethod) {
        this.requestedMethod = requestedMethod;
        return this;
    }

    public AutorenewalTrackingBuilder setRequester(String requester) {
        this.requester = requester;
        return this;
    }

    public AutorenewalTrackingBuilder setId(UUID id) {
        this.id = id;
        return this;
    }

    public AutorenewalTrackingBuilder setInterfaceId(int interfaceId) {
        this.interfaceId = interfaceId;
        return this;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getAutoRenewalOperation() {
        return autoRenewalOperation;
    }

    public String getRequestedMethod() {
        return requestedMethod;
    }

    public String getRequester() {
        return requester;
    }

    public UUID getId() {
        return id;
    }

    public int getInterfaceId() {
        return interfaceId;
    }
}
