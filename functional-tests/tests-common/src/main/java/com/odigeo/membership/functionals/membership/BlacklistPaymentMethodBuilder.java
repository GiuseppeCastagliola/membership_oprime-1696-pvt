package com.odigeo.membership.functionals.membership;

public class BlacklistPaymentMethodBuilder {

    private String id;
    private String timestamp;
    private String errorType;
    private String errorMessage;
    private String membershipId;

    public String getId() {
        return id;
    }

    public BlacklistPaymentMethodBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public BlacklistPaymentMethodBuilder withTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getErrorType() {
        return errorType;
    }

    public BlacklistPaymentMethodBuilder withErrorType(String errorType) {
        this.errorType = errorType;
        return this;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public BlacklistPaymentMethodBuilder withErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public BlacklistPaymentMethodBuilder withMembershipId(String membershipId) {
        this.membershipId = membershipId;
        return this;
    }
}
