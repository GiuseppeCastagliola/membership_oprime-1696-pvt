package com.odigeo.membership.functionals.membership;

import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;

public class CheckMemberOnPassengerListRequestBuilder {

    private Long userId;
    private String site;

    public CheckMemberOnPassengerListRequestBuilder setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public CheckMemberOnPassengerListRequestBuilder setSite(String site) {
        this.site = site;
        return this;
    }

    public CheckMemberOnPassengerListRequest build() {
        CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest = new CheckMemberOnPassengerListRequest();
        checkMemberOnPassengerListRequest.setUserId(this.userId);
        checkMemberOnPassengerListRequest.setSite(this.site);
        return checkMemberOnPassengerListRequest;
    }
}
