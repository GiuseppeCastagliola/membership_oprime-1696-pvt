package com.odigeo.membership.functionals.membership;

import java.math.BigDecimal;

public class MembershipProductFeeBuilder {

    private String membershipId;
    private BigDecimal amount;
    private String currency;
    private String feeType;

    public String getMembershipId() {
        return membershipId;
    }

    public MembershipProductFeeBuilder setMembershipId(String membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public MembershipProductFeeBuilder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public MembershipProductFeeBuilder setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getFeeType() {
        return feeType;
    }

    public MembershipProductFeeBuilder setFeeType(String feeType) {
        this.feeType = feeType;
        return this;
    }
}
