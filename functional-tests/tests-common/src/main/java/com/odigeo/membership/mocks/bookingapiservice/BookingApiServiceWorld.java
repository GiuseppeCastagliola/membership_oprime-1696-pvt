package com.odigeo.membership.mocks.bookingapiservice;

import com.google.inject.Inject;
import com.odigeo.membership.mocks.bookingsearchapiservice.BookingSearchApiServiceHttpServer;
import com.odigeo.membership.mocks.bookingsearchapiservice.BookingSearchApiServiceMock;
import com.odigeo.membership.server.ServerStopException;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;

@ScenarioScoped
public class BookingApiServiceWorld {

    private final BookingSearchApiServiceMock bookingSearchApiServiceMock;
    private final BookingSearchApiServiceHttpServer bookingSearchApiServiceHttpServer;

    private final BookingApiServiceMock bookingApiServiceMock;
    private final BookingApiServiceHttpServer bookingApiServiceHttpServer;

    @Inject
    public BookingApiServiceWorld(BookingApiServiceMock bookingApiServiceMock, BookingApiServiceHttpServer bookingApiServiceHttpServer,
                                  BookingSearchApiServiceMock bookingSearchApiServiceMock, BookingSearchApiServiceHttpServer bookingSearchApiServiceHttpServer) {
        this.bookingApiServiceMock = bookingApiServiceMock;
        this.bookingApiServiceHttpServer = bookingApiServiceHttpServer;
        this.bookingSearchApiServiceMock = bookingSearchApiServiceMock;
        this.bookingSearchApiServiceHttpServer = bookingSearchApiServiceHttpServer;
    }

    public void install() throws ServerStopException {
        if (bookingApiServiceHttpServer.serverNotCreated()) {
            bookingApiServiceHttpServer.startServer();
        }
        bookingApiServiceHttpServer.addService(bookingApiServiceMock);

        if (bookingSearchApiServiceHttpServer.serverNotCreated()) {
            bookingSearchApiServiceHttpServer.startServer();
        }
        bookingSearchApiServiceHttpServer.addService(bookingSearchApiServiceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(bookingApiServiceHttpServer)) {
            bookingApiServiceHttpServer.clearServices();
        }

        if (Objects.nonNull(bookingSearchApiServiceHttpServer)) {
            bookingSearchApiServiceHttpServer.clearServices();
        }
    }

    public BookingApiServiceMock getBookingApiServiceMock() {
        return bookingApiServiceMock;
    }

    public BookingSearchApiServiceMock getBookingSearchApiServiceMock() {
        return bookingSearchApiServiceMock;
    }

}
