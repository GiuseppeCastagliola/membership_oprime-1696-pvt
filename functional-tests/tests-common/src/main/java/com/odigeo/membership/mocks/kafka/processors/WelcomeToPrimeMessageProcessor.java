package com.odigeo.membership.mocks.kafka.processors;

import com.google.inject.Singleton;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class WelcomeToPrimeMessageProcessor extends AbstractKafkaMessageProcessor<MembershipMailerMessage> {
    private static final Logger LOGGER = Logger.getLogger(WelcomeToPrimeMessageProcessor.class);
    private final List<MembershipMailerMessage> receivedMessages = new ArrayList<>();

    @Override
    public void onMessage(MembershipMailerMessage message) {
        LOGGER.info("Received new WelcomeToPrime request message from kafka for membershipId: " + message.getMembershipId());
        receivedMessages.add(message);
    }

    public List<MembershipMailerMessage> getReceivedMessages() {
        return receivedMessages;
    }

    public void resetMessageList() {
        receivedMessages.clear();
    }
}
