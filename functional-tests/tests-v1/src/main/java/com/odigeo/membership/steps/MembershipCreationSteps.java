package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.MemberAccountBuilder;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import com.odigeo.membership.mocks.database.stores.MembershipFeeStore;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipCreationSteps extends CommonSteps {

    private final MembershipManagementWorld world;
    private final DatabaseWorld databaseWorld;

    @Inject
    public MembershipCreationSteps(MembershipManagementWorld world, DatabaseWorld databaseWorld, ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
        this.world = world;
        this.databaseWorld = databaseWorld;
    }

    @And("^all information for a pending to collect is filled in the membership$")
    public void allInfoForPendingToCollectIsFilled() throws InterruptedException, SQLException, ClassNotFoundException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
        MembershipBuilder membershipBuilder = databaseWorld.getMembershipById(world.getCreatedMemberId());
        final LocalDate expirationDate = LocalDate.parse(membershipBuilder.getExpirationDate(), formatter);
        assertNull(membershipBuilder.getActivationDate());
        assertNull(membershipBuilder.getBalance());
        assertEquals(membershipBuilder.getCurrencyCode(), "EUR");
        assertEquals(membershipBuilder.getMonthsDuration().intValue(), 12);
        assertEquals(membershipBuilder.getSourceType(), "FUNNEL_BOOKING");
        assertEquals(membershipBuilder.getMembershipType(), "BASIC");
        assertEquals((long) membershipBuilder.getMemberAccountId(), 1234L);
        assertEquals(expirationDate.toString(), "2023-02-04");
        assertEquals(membershipBuilder.getTotalPrice(), BigDecimal.valueOf(49.99));
        assertEquals(membershipBuilder.getWebsite(), "ES");
        assertEquals(membershipBuilder.getProductStatus(), "INIT");
    }

    @And("^the recurringId (\\w+) has been inserted for the new membership$")
    public void recurringIdHasBeenInserted(String expectedRecurringId) throws InterruptedException, SQLException, ClassNotFoundException {
        Optional<String> recurringId = databaseWorld.getRecurringByMembershipId(world.getCreatedMemberId());
        assertTrue(recurringId.isPresent());
        assertEquals(recurringId.get(), expectedRecurringId);
    }

    @And("^the membership fee has been inserted with right information$")
    public void membershipFeeHasBeenInserted() throws InterruptedException, SQLException, ClassNotFoundException {
        Optional<MembershipFeeStore.MembershipFee> membershipFees = databaseWorld
            .getMembershipFeesByMembershipId(world.getCreatedMemberId());
        assertTrue(membershipFees.isPresent());
        assertEquals(membershipFees.get().getAmount(), BigDecimal.valueOf(49.99));
        assertEquals(membershipFees.get().getCurrency(), "EUR");
        assertEquals(membershipFees.get().getFeeType(), "MEMBERSHIP_RENEWAL");
    }

    @Then("^no new membership in pending to collect was created for memberAccount (\\d+) and website (\\w+)$")
    public void onlyOneMembershipPendingToCollectExists(long memberAccountId, String website)
        throws InterruptedException, SQLException, ClassNotFoundException {
        List<MembershipBuilder> membershipsList = databaseWorld.getMembershipByMemberAccountId(memberAccountId);
        long numOfMembershipsMatches = membershipsList.stream()
            .filter(membershipBuilder -> "PENDING_TO_COLLECT".equals(membershipBuilder.getStatus()) && website.equals(membershipBuilder.getWebsite()))
            .count();
        assertEquals(numOfMembershipsMatches, 1);
    }

    @And("^the membershipId returned is (\\d+)$")
    public void theMembershipIdReturned(final Long expectedMembershipId) {
        assertEquals(world.getCreatedMemberId(), expectedMembershipId);
    }

    @Then("^the membership belongs to a user with id (\\d+)$")
    public void theMembershipBelongsToUser(Long userId) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilder = databaseWorld.getMembershipById(world.getCreatedMemberId());
        assertNotNull(membershipBuilder.getMemberAccountId());
        MemberAccountBuilder memberAccountBuilder = databaseWorld.getMemberAccountByMemberAccountId(membershipBuilder.getMemberAccountId());
        assertEquals(memberAccountBuilder.getUserId(), userId);
    }
}
