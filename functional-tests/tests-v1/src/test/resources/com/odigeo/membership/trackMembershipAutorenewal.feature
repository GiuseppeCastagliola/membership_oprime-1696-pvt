Feature: Track auto-renewal operations when update membership

  Scenario Outline: Track auto-renewal operation when use the membership update service
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 2206            | 2207   | FULANITO  | DE TAL    |
    And the next membership stored in db:
      | memberId       | website | status             | autoRenewal   | memberAccountId | activationDate | expirationDate | balance |
      | <membershipId> | ES      | <membershipStatus> | <autoRenewal> | 2206            | 2020-11-30     | 2020-11-30     | 0       |
    When requested to update membership
      | membershipId   | operation   |
      | <membershipId> | <operation> |
    Then the <autoRenewalOperation> auto-renewal operation tracking was eventually stored in db for membership with ID <membershipId>
    Examples:
      | membershipId | membershipStatus | operation             | autoRenewal | autoRenewalOperation |
      | 2205         | ACTIVATED        | DISABLE_AUTO_RENEWAL  | ENABLED     | DISABLE_AUTO_RENEW   |
      | 2205         | ACTIVATED        | ENABLE_AUTO_RENEWAL   | DISABLED    | ENABLE_AUTO_RENEW    |
      | 2205         | ACTIVATED        | DEACTIVATE_MEMBERSHIP | ENABLED     | DISABLE_AUTO_RENEW   |
      | 2205         | DEACTIVATED      | REACTIVATE_MEMBERSHIP | DISABLED    | ENABLE_AUTO_RENEW    |
