Feature: Send updated memberships to Kafka for reporter

  Scenario Outline: Send Kafka message when update membership
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 9938            | 1010   | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId       | website | status             | autoRenewal   | memberAccountId | activationDate | expirationDate | balance |
      | <membershipId> | ES      | <membershipStatus> | <autoRenewal> | 9938            | 2019-10-06     | 2020-10-06     | 0       |
    And property to send IDs to Kafka has value true in db:
    When requested to update membership
      | membershipId   | operation   |
      | <membershipId> | <operation> |
    Then the result of the membership update operation is true
    And membershipUpdateMessage is correctly sent to kafka queue with the membershipId <membershipId>
    Examples:
      | membershipId | membershipStatus | operation             | autoRenewal |
      | 8375         | ACTIVATED        | DEACTIVATE_MEMBERSHIP | ENABLED     |
      | 8876         | DEACTIVATED      | REACTIVATE_MEMBERSHIP | ENABLED     |
      | 8377         | ACTIVATED        | DISABLE_AUTO_RENEWAL  | ENABLED     |
      | 8378         | ACTIVATED        | ENABLE_AUTO_RENEWAL   | DISABLED    |
      | 8379         | ACTIVATED        | EXPIRE_MEMBERSHIP     | ENABLED     |