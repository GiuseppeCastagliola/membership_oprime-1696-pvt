package com.odigeo.membership;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum MemberStatus {
    PENDING_TO_ACTIVATE,
    PENDING_TO_COLLECT,
    ACTIVATED,
    EXPIRED,
    DEACTIVATED,
    DISCARDED,
    UNKNOWN;

    static {
        PENDING_TO_ACTIVATE.allowedStatusTransitions = Arrays.asList(MemberStatus.ACTIVATED, MemberStatus.EXPIRED);
        PENDING_TO_COLLECT.allowedStatusTransitions = Arrays.asList(MemberStatus.ACTIVATED, MemberStatus.EXPIRED, MemberStatus.DISCARDED);
        ACTIVATED.allowedStatusTransitions = Arrays.asList(MemberStatus.EXPIRED, MemberStatus.DEACTIVATED);
        EXPIRED.allowedStatusTransitions = Collections.emptyList();
        DEACTIVATED.allowedStatusTransitions = Arrays.asList(MemberStatus.ACTIVATED, MemberStatus.EXPIRED);
        DISCARDED.allowedStatusTransitions = Collections.emptyList();
        UNKNOWN.allowedStatusTransitions = Collections.emptyList();
    }

    private List<MemberStatus> allowedStatusTransitions;

    public boolean isStatusTransitionAllowed(MemberStatus toStatus) {
        return this.allowedStatusTransitions.contains(toStatus);
    }

    public boolean is(MemberStatus memberStatus) {
        return this.equals(memberStatus);
    }

    public boolean isNot(MemberStatus memberStatus) {
        return !(this.equals(memberStatus));
    }
}
