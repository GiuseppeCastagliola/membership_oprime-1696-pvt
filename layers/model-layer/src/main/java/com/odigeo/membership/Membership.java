package com.odigeo.membership;

import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Membership implements Serializable {

    private final Long id;
    private final String website;
    private final MemberStatus status;
    private final MembershipRenewal autoRenewal;
    private final LocalDateTime activationDate;
    private LocalDateTime expirationDate;
    private final LocalDateTime timestamp;
    private Boolean isBookingLimitReached;
    private final long memberAccountId;
    private BigDecimal balance;
    private final int monthsDuration;
    private final MembershipType membershipType;
    private final SourceType sourceType;
    private final ProductStatus productStatus;
    private final BigDecimal totalPrice;
    private final BigDecimal renewalPrice;
    private final Integer renewalDuration;
    private final String currencyCode;
    private final String recurringId;
    private final List<MemberStatusAction> memberStatusActions;
    private final List<MembershipRecurring> membershipRecurring;
    private final MemberAccount memberAccount;

    public Membership(MembershipBuilder membershipBuilder) {
        this.id = membershipBuilder.getId();
        this.website = membershipBuilder.getWebsite();
        this.status = membershipBuilder.getStatus();
        this.autoRenewal = membershipBuilder.getMembershipRenewal();
        this.activationDate = membershipBuilder.getActivationDate();
        this.expirationDate = membershipBuilder.getExpirationDate();
        this.memberAccountId = membershipBuilder.getMemberAccountId();
        this.membershipType = membershipBuilder.getMembershipType();
        this.monthsDuration = membershipBuilder.getMonthsDuration();
        this.balance = membershipBuilder.getBalance();
        this.sourceType = membershipBuilder.getSourceType();
        this.productStatus = membershipBuilder.getProductStatus();
        MembershipPrices membershipPrices = membershipBuilder.getMembershipPricesBuilder().build();
        this.totalPrice = membershipPrices.getTotalPrice();
        this.renewalPrice = membershipPrices.getRenewalPrice();
        this.renewalDuration = membershipBuilder.getRenewalDuration();
        this.currencyCode = membershipPrices.getCurrencyCode();
        this.recurringId = membershipBuilder.getRecurringId();
        this.timestamp = membershipBuilder.getTimestamp();
        this.memberStatusActions = Optional.ofNullable(membershipBuilder.getMemberStatusActions()).orElseGet(ArrayList::new);
        this.membershipRecurring = Optional.ofNullable(membershipBuilder.getMembershipRecurring()).orElseGet(ArrayList::new);
        this.memberAccount = membershipBuilder.getMemberAccount();
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public Integer getRenewalDuration() {
        return renewalDuration;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public ProductStatus getProductStatus() {
        return this.productStatus;
    }

    public Boolean getIsActive() {
        return (status == MemberStatus.ACTIVATED);
    }

    public Boolean getBookingLimitReached() {
        return isBookingLimitReached;
    }

    public Membership setBookingLimitReached(Boolean bookingLimitReached) {
        isBookingLimitReached = bookingLimitReached;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getWebsite() {
        return website;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public MembershipRenewal getAutoRenewal() {
        return autoRenewal;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getMemberAccountId() {
        return memberAccountId;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public MembershipType getMembershipType() {
        return membershipType;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public List<MemberStatusAction> getMemberStatusActions() {
        return memberStatusActions;
    }

    public List<MembershipRecurring> getMembershipRecurring() {
        return membershipRecurring;
    }

    public MemberAccount getMemberAccount() {
        return memberAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Membership) || getClass() != o.getClass()) {
            return false;
        }

        Membership that = (Membership) o;
        return (long) id == (long) that.id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id)
                .toHashCode();
    }

    public boolean isRenewable() {
        return MemberStatus.ACTIVATED.equals(this.getStatus()) && MembershipRenewal.ENABLED.equals(this.getAutoRenewal());
    }
}
