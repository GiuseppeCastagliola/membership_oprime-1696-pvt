package com.odigeo.membership.auth;

import java.util.Objects;

public class PermissionResponse {
    private String userId;
    private AppPermission perAppPermissions;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AppPermission getPerAppPermissions() {
        return perAppPermissions;
    }

    public void setPerAppPermissions(AppPermission perAppPermissions) {
        this.perAppPermissions = perAppPermissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PermissionResponse that = (PermissionResponse) o;
        return Objects.equals(userId, that.userId)
                && Objects.equals(perAppPermissions, that.perAppPermissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, perAppPermissions);
    }
}
