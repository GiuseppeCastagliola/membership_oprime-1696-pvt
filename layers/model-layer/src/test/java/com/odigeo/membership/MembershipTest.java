package com.odigeo.membership;

import bean.test.BeanTest;
import com.odigeo.membership.enums.MembershipType;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MembershipTest extends BeanTest<Membership> {

    private static final Long MEMBER_ID = 1234L;
    private static final Long MEMBER_ACCOUNT_ID = 1L;
    private static final Integer RENEWAL_DURATION = 12;
    private static final Long USER_ID = 232L;
    private static final String WEBSITE = "EDES";
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, "test", "test");
    private static final MemberStatus MEMBER_STATUS_ACTIVATED = MemberStatus.ACTIVATED;
    private static final MemberStatus MEMBER_STATUS_PENDING = MemberStatus.PENDING_TO_ACTIVATE;
    private static final MemberStatus MEMBER_STATUS_DEACTIVATED = MemberStatus.DEACTIVATED;

    private static final MembershipRenewal MEMBERSHIP_RENEWAL = MembershipRenewal.ENABLED;
    private static final MembershipRenewal MEMBERSHIP_RENEWAL_DISABLED = MembershipRenewal.DISABLED;
    private static final int DEFAULT_MONTHS_DURATION = 12;

    @Override
    protected Membership getBean() {
        return new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_ACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL).setRenewalDuration(RENEWAL_DURATION)
                .setMemberAccountId(MEMBER_ACCOUNT_ID).setMonthsDuration(DEFAULT_MONTHS_DURATION).setMembershipType(MEMBERSHIP_TYPE)
                .setProductStatus(ProductStatus.CONTRACT).setMemberAccount(MEMBER_ACCOUNT)
                .setExpirationDate(LocalDateTime.now()).build();
    }

    @Test
    protected void testMemberIsActive() {
        Membership membership = new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_ACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setMembershipType(MEMBERSHIP_TYPE).setMonthsDuration(DEFAULT_MONTHS_DURATION).setProductStatus(ProductStatus.CONTRACT).build();
        assertTrue(membership.getIsActive());
    }

    @Test
    protected void testMemberIsNotActive() {
        Membership membership = new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_PENDING).setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setMembershipType(MEMBERSHIP_TYPE).setMonthsDuration(DEFAULT_MONTHS_DURATION).setProductStatus(ProductStatus.INIT).build();
        assertFalse(membership.getIsActive());
    }

    @Test
    protected void testMemberDeactivated() {
        Membership membership = new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_DEACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setMembershipType(MEMBERSHIP_TYPE).setMonthsDuration(DEFAULT_MONTHS_DURATION).setProductStatus(ProductStatus.CONTRACT).build();
        assertFalse(membership.getIsActive());
    }

    @Test
    protected void testEquals() {
        Membership membership = new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_ACTIVATED)
                .setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
        Membership membershipDup = new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_ACTIVATED)
                .setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
        assertEquals(membership, membershipDup);
        assertEquals(membership, membership);
        Membership membershipDiff = new MembershipBuilder().setId(MEMBER_ID + 1).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_ACTIVATED)
                .setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
        assertNotEquals(membership, membershipDiff);
        assertNotEquals(membershipDup, membershipDiff);
    }

    @Test
    protected void testHashCode() {
        Membership membership = new MembershipBuilder().setId(MEMBER_ID).setWebsite(WEBSITE).setStatus(MEMBER_STATUS_ACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL).setMemberAccountId(MEMBER_ACCOUNT_ID).setMembershipType(MEMBERSHIP_TYPE).setProductStatus(ProductStatus.CONTRACT).build();
        assertNotNull(membership.hashCode());
    }

    @Test
    protected void testIsRenewable() {
        Membership membershipRenewable = new MembershipBuilder().setStatus(MEMBER_STATUS_ACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL).build();
        Membership membershipNotRenewable1 = new MembershipBuilder().setStatus(MEMBER_STATUS_ACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL_DISABLED).build();
        Membership membershipNotRenewable2 = new MembershipBuilder().setStatus(MEMBER_STATUS_DEACTIVATED).setMembershipRenewal(MEMBERSHIP_RENEWAL).build();
        assertTrue(membershipRenewable.isRenewable());
        assertFalse(membershipNotRenewable1.isRenewable());
        assertFalse(membershipNotRenewable2.isRenewable());
    }
}
