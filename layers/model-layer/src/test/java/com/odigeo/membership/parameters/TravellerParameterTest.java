package com.odigeo.membership.parameters;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TravellerParameterTest {

    @Test
    public void testConstructor() {

        final String name = "NAME";
        final String lastNames = "LASTNAMES";

        TravellerParameter traveller = new TravellerParameter();
        traveller.setName(name);
        traveller.setLastNames(lastNames);

        Assert.assertEquals(traveller.getName(),name);
        Assert.assertEquals(traveller.getLastNames(),lastNames);
    }

}
