package com.odigeo.product.membership.service;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.common.base.Strings;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.product.membership.service.util.ProductServiceMappingV2;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.response.exception.MembershipProductServiceException;
import com.odigeo.product.response.exception.MembershipProductServiceExceptionType;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.CloseProductProviderRequest;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.enums.CloseActionResult;
import com.odigeo.product.v2.model.enums.ProductType;
import com.odigeo.product.v2.model.enums.ProviderProductStatus;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public abstract class ProductServiceV2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceV2.class);
    private static final String MESSAGE_LOG_DEFAULT_CALL_FUNCTION = "Call function: {} with ProductId: {} and TransactionProductId {}";
    private static final String MESSAGE_LOG_PRODUCT_IDENTIFIER_NOT_FOUND = "The product identifier must not be empty or null";
    private static final String MEMBERSHIP_RENEWAL_FEE_SUBCODE = "AE10";
    static final String MESSAGE_LOG_DEFAULT_EXCEPTION = "Caught exception: {}";
    static final String ROLLBACK_TRANSACTION_PRODUCT = "rollbackTransactionProduct";
    static final String CANCEL_TRANSACTION_PRODUCT = "cancelTransactionProduct";
    static final String SUSPEND_TRANSACTION_PRODUCT = "suspendTransactionProduct";
    static final String RESUME_TRANSACTION_PRODUCT = "resumeTransactionProduct";

    private final MemberService memberService;
    private final MembershipProductService membershipProductService;

    public ProductServiceV2(MemberService memberService,
                            MembershipProductService membershipProductService) {
        this.memberService = memberService;
        this.membershipProductService = membershipProductService;
    }

    public abstract Product getProduct(String productId) throws ProductException;

    public abstract void saveProduct(String productId, String productProviderId, boolean isPartial) throws ProductException;

    void logAndValidateProductId(String function, String productId, String transactionProductId) throws ProductException {
        defaultLoggerMessage(function, productId, transactionProductId);
        validateProductId(productId);
    }

    protected void defaultLoggerMessage(String function, String productId, String transactionProductId) {
        LOGGER.info(MESSAGE_LOG_DEFAULT_CALL_FUNCTION,
                function,
                productId,
                transactionProductId);
    }

    private void validateProductId(String productId) throws ProductException {
        if (Strings.isNullOrEmpty(productId)) {
            LOGGER.error(MESSAGE_LOG_PRODUCT_IDENTIFIER_NOT_FOUND);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.BAD_REQUEST, productId, ProductType.MEMBERSHIP_RENEWAL, MESSAGE_LOG_PRODUCT_IDENTIFIER_NOT_FOUND);
        }
    }

    Product getProductFromDatabase(String productId) throws MissingElementException, DataAccessException {
        Membership membership = memberService.getMembershipById(Long.parseLong(productId));
        List<MembershipFee> membershipFeeList = membershipProductService.getMembershipFees(Long.parseLong(productId));
        membershipFeeList.forEach(m -> m.setSubCode(MEMBERSHIP_RENEWAL_FEE_SUBCODE));
        return ProductServiceMappingV2.asProduct(membership, membershipFeeList.get(0));
    }

    protected void validateProductInContract(Membership membership) throws ProductException {
        ProductStatus productStatus = membership.getProductStatus();
        if (ProductStatus.CONTRACT != productStatus) {
            String messageError = "Can't activate a membership that is not in CONTRACT status. Id: " + membership.getId() + ", status: " + productStatus;
            LOGGER.error(messageError);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.FORBIDDEN, String.valueOf(membership.getId()), ProductType.MEMBERSHIP_RENEWAL, messageError);
        }
    }

    public void commitTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId("commitTransactionProduct", productId, transactionProductId);

        try {
            Membership membershipToCommit = getMemberService().getMembershipById(Long.parseLong(productId));
            ProductStatus productStatus = membershipToCommit.getProductStatus();
            if (!ProductStatus.INIT.equals(productStatus)) {
                String messageError = "Can't commit an order that is not in INIT status. Id: " + productId + ", status: " + productStatus;
                LOGGER.error(messageError);
                throw new MembershipProductServiceException(MembershipProductServiceExceptionType.FORBIDDEN, productId, ProductType.MEMBERSHIP_RENEWAL, messageError);
            }
            if (commitMembership(membershipToCommit)) {
                MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.RENEWAL_CONTRACT_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
            }
        } catch (MissingElementException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        } catch (DataAccessException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        }
    }

    protected abstract boolean commitMembership(Membership membership) throws MissingElementException, DataAccessException;

    public void rollbackTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(ROLLBACK_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public void cancelTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(CANCEL_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public void suspendTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(SUSPEND_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public void resumeTransactionProduct(String productId, String transactionProductId) throws ProductException {
        logAndValidateProductId(RESUME_TRANSACTION_PRODUCT, productId, transactionProductId);
    }

    public BookingApiMembershipInfo getBookingApiProductInfo(String productId) throws ProductException {
        LOGGER.info("Call function: getBookingApiProductInfo with ProductId: {}", productId);
        validateProductId(productId);
        try {
            Membership membership = memberService.getMembershipById(Long.parseLong(productId));
            List<MembershipFee> membershipFeeList = membershipProductService.getMembershipFees(Long.parseLong(productId));
            return ProductServiceMappingV2.asBookingApiMembershipInfo(membership, membershipFeeList.get(0));
        } catch (DataNotFoundException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.NOT_FOUND, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        } catch (MissingElementException | DataAccessException e) {
            LOGGER.error(MESSAGE_LOG_DEFAULT_EXCEPTION, e.getMessage(), e);
            throw new MembershipProductServiceException(MembershipProductServiceExceptionType.INTERNAL_SERVER_ERROR, productId, ProductType.MEMBERSHIP_RENEWAL, e.getMessage(), e);
        }
    }

    public CloseProviderProductResponse closeProviderProduct(String productId, String transactionProductId, CloseProductProviderRequest request) {
        LOGGER.info("Call function: closeProviderProduct with ProductId: {}, transactionProductId: {}, request: {}", productId, transactionProductId, request);
        CloseProviderProductResponse closeProviderProductResponse = new CloseProviderProductResponse();
        closeProviderProductResponse.setCloseActionResult(CloseActionResult.OK);
        closeProviderProductResponse.setProviderProductStatus(ProviderProductStatus.CONTRACT);
        closeProviderProductResponse.setRollbackable(false);
        closeProviderProductResponse.setUserInteractionNeeded(null);
        return closeProviderProductResponse;
    }

    MemberService getMemberService() {
        return memberService;
    }
}
