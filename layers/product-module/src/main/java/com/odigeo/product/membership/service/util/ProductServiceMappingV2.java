package com.odigeo.product.membership.service.util;

import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipFee;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Price;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.UserInteractionNeeded;
import com.odigeo.product.v2.model.enums.CloseActionResult;
import com.odigeo.product.v2.model.enums.ProductType;
import com.odigeo.product.v2.model.enums.ProviderProductStatus;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.List;

public final class ProductServiceMappingV2 {

    private ProductServiceMappingV2() {

    }

    private static List<Fee> getFees(BigDecimal price, String currencyCode, String subCode) {
        final List<Fee> fees = new ArrayList<>();
        Fee fee = new Fee();
        fee.setPrice(getPrice(price, currencyCode));
        fee.setCreationDate(new Date());
        fee.setLabel("MARKUP_TAX");
        fee.setSubCode(subCode);
        fees.add(fee);
        return fees;
    }

    public static Product asProduct(Membership membership, MembershipFee membershipFee) {
        Product product = new Product();
        product.setId(String.valueOf(membership.getId()));
        product.setFees(getFees(membershipFee.getAmount(), membershipFee.getCurrency(), membershipFee.getSubCode()));
        product.setMerchantPrice(getPrice(membershipFee.getAmount(), membershipFee.getCurrency()));
        product.setProviderProducts(Collections.emptyList());
        product.setSellingPrice(getPrice(membershipFee.getAmount(), membershipFee.getCurrency()));
        product.setStatus(membership.getProductStatus().name());
        product.setType(ProductType.MEMBERSHIP_RENEWAL.toString());
        return product;
    }

    public static BookingApiMembershipInfo asBookingApiMembershipInfo(Membership membership, MembershipFee membershipFee) {
        BookingApiMembershipInfo bookingApiMembershipInfo = new BookingApiMembershipInfo();
        bookingApiMembershipInfo.setRenewalDate(getDate(membership.getExpirationDate()));
        bookingApiMembershipInfo.setMembershipId(membership.getId());
        bookingApiMembershipInfo.setRenewal(Boolean.TRUE);
        bookingApiMembershipInfo.setCurrencyCode(membershipFee.getCurrency());
        bookingApiMembershipInfo.setPrice(membershipFee.getAmount());
        return bookingApiMembershipInfo;
    }

    public static CloseProviderProductResponse asCloseProviderProductResponse(ProviderProductStatus status) {
        CloseProviderProductResponse closeProviderProductResponse = new CloseProviderProductResponse();
        closeProviderProductResponse.setCloseActionResult(status == ProviderProductStatus.CONTRACT ? CloseActionResult.OK : CloseActionResult.KO);
        closeProviderProductResponse.setProviderProductStatus(status);
        closeProviderProductResponse.setRollbackable(Boolean.FALSE);
        closeProviderProductResponse.setUserInteractionNeeded(new UserInteractionNeeded());

        return closeProviderProductResponse;
    }

    private static Price getPrice(BigDecimal amount, String currency) {
        Price price = new Price();
        price.setAmount(amount);
        price.setCurrency(Currency.getInstance(currency));
        return price;
    }

    private static Date getDate(LocalDateTime dateToConvert) {
        if (dateToConvert != null) {
            return Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
        }
        return null;
    }

}
