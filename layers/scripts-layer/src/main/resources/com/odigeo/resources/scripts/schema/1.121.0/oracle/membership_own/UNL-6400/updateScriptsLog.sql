INSERT into GE_SCRIPTS_LOG (ID,EXECUTION_DATE,MODULE) values ('UNL-6400/00_create_table_ge_membership_auto_renewal_tracking.sql',systimestamp,'membership');
INSERT into GE_SCRIPTS_LOG (ID,EXECUTION_DATE,MODULE) values ('UNL-6400/01_create_index_ge_auto_renewal_tracking.sql',systimestamp,'membership');
INSERT into GE_SCRIPTS_LOG (ID,EXECUTION_DATE,MODULE) values ('rollback/R00_create_index_ge_auto_renewal_tracking.sql',systimestamp,'membership');
INSERT into GE_SCRIPTS_LOG (ID,EXECUTION_DATE,MODULE) values ('rollback/R01_create_table_ge_membership_auto_renewal_tracking.sql',systimestamp,'membership');
