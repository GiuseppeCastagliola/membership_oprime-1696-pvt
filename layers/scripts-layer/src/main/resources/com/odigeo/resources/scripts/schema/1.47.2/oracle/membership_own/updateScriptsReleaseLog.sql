UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.47.2' where ID ='UNL-5265/01.create_recurring_info_table.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.47.2' where ID ='UNL-5265/02.create_recurring_info_sequence.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.47.2' where ID ='rollback/R_01.create_recurring_info_table.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.47.2' where ID ='rollback/R_02.create_recurring_info_sequence.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.47.2',systimestamp,'membership');
