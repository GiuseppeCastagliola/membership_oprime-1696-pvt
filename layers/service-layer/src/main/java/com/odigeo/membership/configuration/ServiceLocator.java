package com.odigeo.membership.configuration;

/**
 * Interface to provide access to the different services.
 * <p/>
 * Several implementations may exist, depending on the execution environment.
 */
public interface ServiceLocator {

    /**
     * Return an instance of a service of a certain type.
     *
     * @param serviceType
     * @param <T>
     * @return
     * @throws UnavailableServiceException
     * @deprecated use ConfigurationEngine instead
     */
    @Deprecated
    <T> T getService(Class<T> serviceType) throws UnavailableServiceException;

}
