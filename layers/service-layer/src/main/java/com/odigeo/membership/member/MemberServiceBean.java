package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.creation.MembershipCreationService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Stateless
@Local(MemberService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MemberServiceBean extends AbstractServiceBean implements MemberService {

    private MembershipCreationService membershipCreationService;

    @Override
    public Membership getMembershipById(long membershipId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipById(dataSource, membershipId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + membershipId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + membershipId, e);
        }
    }

    @Override
    public Membership getMembershipByIdWithMemberAccount(long membershipId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipByIdWithMemberAccount(dataSource, membershipId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + membershipId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + membershipId, e);
        }
    }

    @Override
    public List<Membership> getMembershipsByAccountId(long memberAccountId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipByMemberAccountId(dataSource, memberAccountId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Memberships not found by memberAccountId: " + memberAccountId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load memberships with memberAccountId: " + memberAccountId, e);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Boolean activateMembership(long membershipId, long bookingId, BigDecimal balance) throws DataAccessException {
        Optional<Membership> activatedMembership;
        try {
            activatedMembership = getMembershipCreationService().activate(membershipId, balance);
        } catch (SQLException | MissingElementException e) {
            throw new DataAccessRollbackException("Cannot activate membership for membershipId " + membershipId + " .Error Status Action ACTIVATION", e);
        }

        activatedMembership.ifPresent(m -> {
            sendActivateMessages(bookingId, m);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.ACTIVATIONS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
        });

        return activatedMembership.isPresent();
    }

    private void sendActivateMessages(long bookingId, Membership activatedMembership) {
        MembershipMessageSendingManager membershipMessageSendingManager = getMembershipMessageSendingManager();
        membershipMessageSendingManager.sendMembershipIdToMembershipReporter(activatedMembership.getId());
        membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(activatedMembership, SubscriptionStatus.SUBSCRIBED);
        membershipMessageSendingManager.sendWelcomeToPrimeMessageToMembershipTransactionalTopic(activatedMembership, bookingId, false);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Long createMembership(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException {
        Long membershipId = getMembershipCreationService().create(membershipCreation);
        getMembershipMessageSendingManager().sendMembershipIdToMembershipReporter(membershipId);
        return membershipId;
    }

    @Override
    public boolean existsMembership(long memberId) throws DataAccessException {
        try {
            return getMembershipStore().fetchMembershipById(dataSource, memberId, true).isPresent();
        } catch (DataNotFoundException | SQLException e) {
            throw new DataAccessException("There was an error trying to determine whether a membership with id " + memberId + " already exists", e);
        }
    }

    private MembershipCreationService getMembershipCreationService() {
        return membershipCreationService != null ? membershipCreationService
                : (membershipCreationService = ConfigurationEngine.getInstance(MembershipCreationService.class));
    }

}
