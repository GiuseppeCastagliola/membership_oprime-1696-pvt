package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.product.MembershipSubscriptionConfiguration;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.search.SearchService;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

@Stateless
@Local(MembershipValidationService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MembershipValidationServiceBean extends AbstractServiceBean implements MembershipValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipValidationServiceBean.class);
    private UserApiManager userApiManager;
    private SearchService searchService;
    private MembershipSubscriptionConfiguration membershipSubscriptionConfiguration;
    private final Predicate<Membership> prohibitsFreeTrial = membership ->
            membership.getMembershipType().equals(MembershipType.BASIC)
                    && membership.getSourceType().equals(SourceType.FUNNEL_BOOKING)
                    && membership.getMonthsDuration() == 1
                    && membership.getAutoRenewal().equals(MembershipRenewal.DISABLED);


    @Override
    public Boolean applyMembership(MemberOnPassengerListParameter memberOnPassengerListParameter) throws DataAccessException {
        Long userId = memberOnPassengerListParameter.getUserId();
        List<MemberAccount> memberList = getMemberManager().getMembersWithActivatedMembershipsByUserId(userId, dataSource);
        return MemberServiceHelper.applyMembership(memberList, memberOnPassengerListParameter.getSite(), memberOnPassengerListParameter.getTravellerList());
    }

    @Override
    public Boolean isMembershipActiveOnWebsite(String siteId) {
        List<String> activeSites = getMembershipSubscriptionConfiguration().getActiveSitesList();
        LOGGER.info("Prime markets are {}", activeSites);
        return activeSites.contains(siteId);
    }

    @Override
    public Boolean isMembershipToBeRenewed(Long membershipId) throws MissingElementException, DataAccessException {
        LOGGER.info("Membership to be renewed is {}", membershipId);
        Membership membership = getMemberManager().getMembershipById(dataSource, membershipId);
        return membership.getIsActive() && MemberServiceHelper.isExpirationDateInPast(membership);
    }

    @Override
    public boolean isEligibleForFreeTrial(FreeTrialCandidateRequest freeTrialCandidateRequest) throws DataAccessException {
        String requestWebsite = freeTrialCandidateRequest.getWebsite();
        boolean freeTrialMembershipPermitted = false;
        if (isMembershipActiveOnWebsite(requestWebsite)) {
            UserInfo userInfo = getUserApiManager().getUserInfo(freeTrialCandidateRequest.getEmail(), requestWebsite);
            freeTrialMembershipPermitted = hasNoPreviousMembershipsPreventingFreeTrial(userInfo);
        }
        MetricsUtils.incrementCounter(MetricsBuilder.buildFreeTrialEligibleByWebsite(freeTrialMembershipPermitted, requestWebsite), MetricsNames.METRICS_REGISTRY_NAME);
        return freeTrialMembershipPermitted;
    }

    private boolean hasNoPreviousMembershipsPreventingFreeTrial(UserInfo userInfo) throws DataAccessException {
        Optional<Long> optUserId = userInfo.getUserId();
        return optUserId.isPresent() ? isFreeTrialAbuser(optUserId.get()) : Boolean.TRUE;
    }

    private boolean isFreeTrialAbuser(Long userId) throws DataAccessException {
        MemberAccountSearch memberAccountSearch = new MemberAccountSearch.Builder().userId(userId).withMembership(true).build();
        return getSearchService().searchMemberAccounts(memberAccountSearch).stream()
                .flatMap(memberAccount -> memberAccount.getMemberships().stream())
                .noneMatch(prohibitsFreeTrial);
    }

    private MembershipSubscriptionConfiguration getMembershipSubscriptionConfiguration() {
        if (Objects.isNull(membershipSubscriptionConfiguration)) {
            membershipSubscriptionConfiguration = ConfigurationEngine.getInstance(MembershipSubscriptionConfiguration.class);
        }
        return membershipSubscriptionConfiguration;
    }

    private UserApiManager getUserApiManager() {
        return Optional.ofNullable(userApiManager).orElseGet(() -> {
            userApiManager = ConfigurationEngine.getInstance(UserApiManager.class);
            return userApiManager;
        });
    }

    private SearchService getSearchService() {
        return Optional.ofNullable(searchService).orElseGet(() -> {
            searchService = ConfigurationEngine.getInstance(SearchService.class);
            return searchService;
        });
    }
}
