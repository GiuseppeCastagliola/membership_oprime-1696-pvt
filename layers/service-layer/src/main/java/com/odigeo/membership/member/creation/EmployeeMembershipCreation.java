package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.search.SearchService;
import com.odigeo.messaging.MembershipMessageSendingManager;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class EmployeeMembershipCreation extends MembershipCreationFactory {

    private final SearchService searchService;
    private final MembershipMessageSendingManager membershipMessageSendingManager;

    @Inject
    public EmployeeMembershipCreation(SearchService searchService, MembershipMessageSendingManager membershipMessageSendingManager) {
        this.searchService = searchService;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
    }

    @Override
    public Long createMembership(DataSource dataSource, MembershipCreation membershipCreation) throws DataAccessException {
        try {
            existActivatedMembershipForUserCheck(membershipCreation, searchService);
            membershipCreation.setActivationDate(LocalDateTime.now());
            membershipCreation.setSubscriptionPrice(BigDecimal.ZERO);
            membershipCreation.setAutoRenewal(MembershipRenewal.DISABLED);
            membershipCreation.setBalance(BigDecimal.ZERO);
            Long memberId = getMemberManager().createMember(dataSource, membershipCreation);
            storeMemberStatusAction(dataSource, memberId, StatusAction.INTERNAL_CREATION);
            membershipMessageSendingManager.sendMembershipIdToMembershipReporter(memberId);
            return memberId;
        } catch (DataAccessException | ActivatedMembershipException e) {
            throw new DataAccessRollbackException("Error creating membership for userId " + membershipCreation.getMemberAccountCreation().getUserId(), e);
        }
    }

    private MemberManager getMemberManager() {
        return ConfigurationEngine.getInstance(MemberManager.class);
    }
}
