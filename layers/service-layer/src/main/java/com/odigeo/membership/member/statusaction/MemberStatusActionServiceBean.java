package com.odigeo.membership.member.statusaction;

import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.member.AbstractServiceBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.sql.SQLException;
import java.util.Optional;

@Stateless
@Local(MemberStatusActionService.class)
public class MemberStatusActionServiceBean extends AbstractServiceBean implements MemberStatusActionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberStatusActionServiceBean.class);

    @Override
    public Optional<MemberStatusAction> lastStatusActionByMembershipId(final Long membershipId) {
        try {
            return getMemberStatusActionStore().selectLastStatusActionByMembershipId(dataSource, membershipId);
        } catch (SQLException e) {
            LOGGER.error("Cannot retrieve the lastStatusAction by membership ID {}", membershipId, e);
            return Optional.empty();
        }
    }
}
