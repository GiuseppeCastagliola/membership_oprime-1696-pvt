package com.odigeo.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.Membership;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.messaging.kafka.MembershipKafkaSender;

import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;

@Singleton
class MembershipMailerMessageService {

    private final MembershipKafkaSender membershipKafkaSender;

    @Inject
    MembershipMailerMessageService(MembershipKafkaSender membershipKafkaSender) {
        this.membershipKafkaSender = membershipKafkaSender;
    }

    void sendWelcomeToPrimeMessageToMembershipTransactionalTopic(Membership membership, long bookingId, boolean force) {
        final MembershipMailerMessage membershipMailerMessage = getMembershipMailerMessage(membership, bookingId);
        membershipKafkaSender.sendMembershipMailerMessageToKafka(membershipMailerMessage, force);
    }

    private MembershipMailerMessage getMembershipMailerMessage(Membership membership, long bookingId) {
        return new MembershipMailerMessage.Builder()
                .withMembershipId(membership.getId())
                .withUserId(membership.getMemberAccount().getUserId())
                .withBookingId(bookingId)
                .withMessageType(WELCOME_TO_PRIME)
                .build();
    }
}
