package com.odigeo.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class MembershipMessageSendingManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipMessageSendingManager.class);

    private final MembershipSubscriptionMessageService membershipSubscriptionMessageService;
    private final MembershipUpdateMessageService membershipUpdateMessageService;
    private final MembershipMailerMessageService membershipMailerMessageService;


    @Inject
    public MembershipMessageSendingManager(MembershipSubscriptionMessageService membershipSubscriptionMessageService,
                                           MembershipUpdateMessageService membershipUpdateMessageService,
                                           MembershipMailerMessageService membershipMailerMessageService) {
        this.membershipSubscriptionMessageService = membershipSubscriptionMessageService;
        this.membershipUpdateMessageService = membershipUpdateMessageService;
        this.membershipMailerMessageService = membershipMailerMessageService;
    }

    public void sendSubscriptionMessageToCRMTopicByRule(MemberStatus previousStatus, MemberStatus newStatus, Membership membership) {
        LOGGER.info("Check validity and send subscription message for membershipId: {}, with status updated from {} to {}", membership.getId(), previousStatus, newStatus);
        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopicByRule(previousStatus, newStatus, membership);
    }

    public boolean sendSubscriptionMessageToCRMTopic(Membership membership, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for membershipId: {}, with subscription status {}", membership.getId(), subscriptionStatus);
        return membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(membership, subscriptionStatus);
    }

    public void sendSubscriptionMessageToCRMTopic(UserInfo userInfo, Membership membership, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for membershipId: {}, with subscription status {}", membership.getId(), subscriptionStatus);
        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(userInfo, membership, subscriptionStatus);
    }

    public void sendSubscriptionMessageToCRMTopic(String email, MembershipCreation membershipCreation, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for email: {}, website: {}, with subscription status {}", email, membershipCreation.getWebsite(), subscriptionStatus);
        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(email, membershipCreation, subscriptionStatus);
    }

    public void sendMembershipIdToMembershipReporter(long membershipId) {
        LOGGER.info("Send update message for membershipId {}", membershipId);
        membershipUpdateMessageService.sendMembershipIdToMembershipReporter(membershipId);
    }

    public void sendWelcomeToPrimeMessageToMembershipTransactionalTopic(Membership membership, long bookingId, boolean force) {
        LOGGER.info("Send mailer message to trigger WelcomeToPrime email for bookingId: {}, by membershipId: {} userId: {}", bookingId, membership.getId(), membership.getMemberAccount().getUserId());
        membershipMailerMessageService.sendWelcomeToPrimeMessageToMembershipTransactionalTopic(membership, bookingId, force);
    }
}
