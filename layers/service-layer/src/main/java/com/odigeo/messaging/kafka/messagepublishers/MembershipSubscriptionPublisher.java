package com.odigeo.messaging.kafka.messagepublishers;

import com.google.inject.Singleton;
import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.MessagePublisher;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import org.apache.kafka.clients.producer.Callback;
import org.apache.log4j.Logger;

@Singleton
public class MembershipSubscriptionPublisher {
    private static final Logger LOGGER = Logger.getLogger(MembershipSubscriptionPublisher.class);

    @MessagePublisher(topic = "MEMBERSHIP_ACTIVATIONS_v4")
    private KafkaPublisher<Message> publisher;

    public void publishMembershipSubscriptionMessage(MembershipSubscriptionMessage membershipSubscriptionMessage, Callback callback) throws PublishMessageException {
        try {
            publisher.publish(membershipSubscriptionMessage, callback);
            LOGGER.info(String.format("MembershipSubscriptionMessage published - email:%s, website:%s",
                    membershipSubscriptionMessage.getEmail(), membershipSubscriptionMessage.getWebsite()));
        } catch (PublishMessageException e) {
            LOGGER.error(String.format("Error publishing MembershipSubscription Message for email: %s, website: %s",
                    membershipSubscriptionMessage.getEmail(), membershipSubscriptionMessage.getWebsite()));
            throw e;
        }
    }
}
