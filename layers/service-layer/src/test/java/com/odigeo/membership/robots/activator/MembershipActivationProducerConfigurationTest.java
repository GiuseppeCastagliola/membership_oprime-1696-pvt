package com.odigeo.membership.robots.activator;

import bean.test.BeanTest;

public class MembershipActivationProducerConfigurationTest extends BeanTest<MembershipActivationProducerConfiguration> {

    @Override
    protected MembershipActivationProducerConfiguration getBean() {
        return new MembershipActivationProducerConfiguration();
    }
}
