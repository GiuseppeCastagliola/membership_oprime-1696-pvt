package com.odigeo.messaging.kafka.messagepublishers;

import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import org.apache.kafka.clients.producer.Callback;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class MembershipUpdatePublisherTest {
    @Mock
    private KafkaPublisher<Message> kafkaPublisher;

    @InjectMocks
    private MembershipUpdatePublisher membershipUpdatePublisher = new MembershipUpdatePublisher();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void publishMessageTest() throws PublishMessageException {
        membershipUpdatePublisher.publishMembershipUpdateMessage(sampleMembershipUpdateMessage(), getCallback());
        verify(kafkaPublisher).publish(any(MembershipUpdateMessage.class), any(Callback.class));
    }

    @Test//(expectedExceptions = PublishMessageException.class)
    public void throwsPublishMessageException() throws PublishMessageException {
        doThrow(new PublishMessageException("PublishMessageException", new RuntimeException())).when(kafkaPublisher).publish(any(MembershipUpdateMessage.class), any());
        membershipUpdatePublisher.publishMembershipUpdateMessage(sampleMembershipUpdateMessage(), getCallback());
    }

    private MembershipUpdateMessage sampleMembershipUpdateMessage() {
        return new MembershipUpdateMessage.Builder()
                .membershipId("11")
                .build();
    }

    private Callback getCallback() {
        return (recordMetadata, e) -> {
        };
    }
}
