package com.odigeo.membership.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.membership.member.nosql.MembershipNoSQLManager;
import com.odigeo.membership.member.nosql.MembershipNoSQLManagerImpl;
import com.odigeo.membership.member.nosql.MembershipNoSQLRepository;
import com.odigeo.membership.member.nosql.MembershipNoSQLStore;
import org.apache.log4j.Logger;

public class MembershipStoreModule extends AbstractModule {

    private static final Logger LOGGER = Logger.getLogger(MembershipStoreModule.class);

    @Override
    protected void configure() {
        LOGGER.info("Starting configure MembershipStoreModule");
        bind(MembershipNoSQLRepository.class).to(MembershipNoSQLStore.class);
        bind(MembershipNoSQLManager.class).to(MembershipNoSQLManagerImpl.class);
        LOGGER.info("Stop configure MembershipStoreModule");
    }
}
