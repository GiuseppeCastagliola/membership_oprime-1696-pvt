package com.odigeo.membership.exception;

import com.edreams.base.DataAccessException;

public class UnavailableDataSourceException extends DataAccessException {

    public UnavailableDataSourceException(String dataSourceName, Throwable th) {
        super(buildMessage(dataSourceName), th);
    }

    private static String buildMessage(String dataSourceName) {
        return "Unavailable DataSource: " + dataSourceName;
    }
}
