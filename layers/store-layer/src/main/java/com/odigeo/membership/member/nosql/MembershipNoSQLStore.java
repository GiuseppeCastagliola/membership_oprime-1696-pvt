package com.odigeo.membership.member.nosql;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.redis.RedisClient;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;
import redis.clients.jedis.params.SetParams;

@Singleton
public class MembershipNoSQLStore implements MembershipNoSQLRepository {

    private static final Logger LOGGER = Logger.getLogger(MembershipNoSQLStore.class);
    private final RedisClient redisClient;

    @Inject
    public MembershipNoSQLStore(RedisClient redisClient) {
        this.redisClient = redisClient;
    }

    @Override
    public void store(String key, String value, int secondsToExpire) throws DataAccessException {
        try (Jedis jedis = redisClient.getJedisConnection()) {
            jedis.set(key, value, SetParams.setParams().ex(secondsToExpire));
        } catch (JedisException e) {
            LOGGER.error("Error connecting with Redis for storing: " + value, e);
            throw new DataAccessException(e);
        }
    }

    @Override
    public String get(String key) throws DataAccessException {
        try (Jedis jedis = redisClient.getJedisConnection()) {
            return jedis.get(key);
        } catch (JedisException e) {
            LOGGER.error("Error connecting in Redis for retrieving the key: " + key, e);
            throw new DataAccessException(e);
        }
    }

}
