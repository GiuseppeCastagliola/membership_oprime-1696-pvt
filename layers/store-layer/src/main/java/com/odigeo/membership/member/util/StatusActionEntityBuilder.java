package com.odigeo.membership.member.util;

import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.StatusAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

public class StatusActionEntityBuilder {

    public static final String ID = "ID";
    private static final String ACTION_DATE = "ACTION_DATE";
    private static final String ACTION_TYPE = "ACTION_TYPE";
    static final String MEMBER_STATUS_ACTION_ID = "MEMBER_STATUS_ACTION_ID";

    MemberStatusAction build(ResultSet rs) throws SQLException {
        Date date = getActionDate(rs.getDate(ACTION_DATE));
        StatusAction statusAction = getStatusActionFromString(rs.getString(ACTION_TYPE));
        return new MemberStatusAction(rs.getLong(MEMBER_STATUS_ACTION_ID), rs.getLong(ID), statusAction, date);
    }

    static Date getActionDate(java.sql.Date actionDate) {
        return Optional.ofNullable(actionDate)
                .map(dbDate -> new Date(dbDate.getTime()))
                .orElse(null);
    }

    static StatusAction getStatusActionFromString(String actionType) {
        return Optional.ofNullable(actionType)
                .filter(StatusAction::contains)
                .map(StatusAction::valueOf)
                .orElse(StatusAction.UNKNOWN);
    }
}
