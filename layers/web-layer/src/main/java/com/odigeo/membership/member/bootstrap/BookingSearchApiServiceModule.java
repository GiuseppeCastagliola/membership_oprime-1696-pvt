package com.odigeo.membership.member.bootstrap;

import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;

public class BookingSearchApiServiceModule extends AbstractRestUtilsModule {

    public BookingSearchApiServiceModule(ServiceNotificator... serviceNotificators) {
        super(BookingSearchApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpBookingSearchApi(BookingSearchApiService.class);
    }
}
