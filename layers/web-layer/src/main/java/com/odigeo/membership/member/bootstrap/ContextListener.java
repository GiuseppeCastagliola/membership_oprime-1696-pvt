package com.odigeo.membership.member.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Module;
import com.google.inject.servlet.ServletModule;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.messaging.KafkaTopicHealthCheck;
import com.odigeo.commons.messaging.MessagePublisherModule;
import com.odigeo.commons.monitoring.dump.ConfiguredBeanDumpState;
import com.odigeo.commons.monitoring.dump.DumpServlet;
import com.odigeo.commons.monitoring.dump.DumpStateRegistry;
import com.odigeo.commons.monitoring.healthcheck.DataSourceHealthCheck;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.monitoring.dump.ServiceDumpState;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import com.odigeo.membership.configuration.MemberServiceModule;
import com.odigeo.membership.configuration.MembershipStoreModule;
import com.odigeo.membership.configuration.services.UserProfileApiHealthChecker;
import com.odigeo.membership.configuration.services.UserProfileApiServiceModule;
import com.odigeo.membership.member.bootstrap.warmup.MembershipControllersWarmUp;
import com.odigeo.membership.member.configuration.JeeResourceLocator;
import com.odigeo.membership.member.monitoring.redis.RedisHealthCheck;
import com.odigeo.tracking.client.context.TrackingClientConfiguration;
import com.odigeo.tracking.client.track.method.TrackingSystemModule;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;
import org.reflections.Reflections;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContextListener implements ServletContextListener {
    private static final String SERVICE_NAME = "MembershipService";
    private static final String APPLICATION_NAME = "membership";
    private static final String BASE_PATH = "com.odigeo.membership";
    private static final Reflections REFLECTIONS = new Reflections(BASE_PATH);
    private static final DumpStateRegistry DUMP_STATE_REGISTRY = new DumpStateRegistry();
    private static final Class[] EXT_SERVICES = {BookingApiService.class, BookingSearchApiService.class};
    private static final String BOOTSTRAP_MESSAGE_INIT = "Bootstrapping .....";
    private static final String BOOTSTRAP_MESSAGE_END = "Bootstrapping finished!";
    private static final String CONFIGURATION_ENGINE = SERVICE_NAME + ": ConfigurationEngine has been initialized";
    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final String MEMBERSHIP_SUBSCRIPTION_TOPIC = "MEMBERSHIP_ACTIVATIONS_v4";
    private static final String MEMBERSHIP_UPDATE_TOPIC = "MEMBERSHIP_UPDATES_V1";
    private static final String WELCOME_EMAIL_TOPIC = "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
        servletContext.log(BOOTSTRAP_MESSAGE_INIT);
        initMetrics();
        List<Module> restUtilsModules = restUtilsModules(servletContext);
        initConfigurationEngine(servletContext, restUtilsModules.toArray(new Module[restUtilsModules.size()]));
        initLog4J(servletContext);
        includeConfigurationsInDump();
        warmUp(servletContext);
        servletContext.setAttribute(DumpServlet.REGISTRY_KEY, DUMP_STATE_REGISTRY);
        servletContext.log(BOOTSTRAP_MESSAGE_END);
    }

    private List<Module> restUtilsModules(ServletContext servletContext) {
        ConfigurationEngine.init();
        final HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();
        healthCheckRegistry.register("com.odigeo.userprofiles.api.v1.UserApiServiceInternal Service", ConfigurationEngine.getInstance(UserProfileApiHealthChecker.class));
        registryHealthChecks(servletContext, healthCheckRegistry);
        AutoHealthCheckRegister autoHealthCheckRegister = new AutoHealthCheckRegister(healthCheckRegistry);
        return assembleModulesList(autoHealthCheckRegister);
    }

    List<Module> assembleModulesList(AutoHealthCheckRegister autoHealthCheckRegister) {
        List<Module> modules = new ArrayList<>();
        modules.add(new MemberServiceModule());
        modules.add(new BookingApiServiceModule(autoHealthCheckRegister));
        modules.add(new BookingSearchApiServiceModule(autoHealthCheckRegister));
        modules.add(new TestAssignmentServiceModule(autoHealthCheckRegister));
        modules.add(new TestDimensionPartitionServiceModule(autoHealthCheckRegister));
        modules.add(new VisitEngineModule(autoHealthCheckRegister));
        modules.add(new UserProfileApiServiceModule());
        modules.add(new UserApiServiceModule());
        modules.add(new TrackingSystemModule(createTrackingConfiguration()));
        modules.add(new MembershipStoreModule());
        modules.add(new MessagePublisherModule());
        modules.add(new ServletModule());
        return modules;
    }

    private void initMetrics() {
        MetricsManager metricsManager = MetricsManager.getInstance();
        metricsManager.changeReporterStatus(ReporterStatus.STARTED);
        metricsManager.addMetricsReporter(MetricsNames.METRICS_REGISTRY_NAME);
        metricsManager.startReporter(MetricsNames.METRICS_REGISTRY_NAME);
    }

    private void registryHealthChecks(ServletContext servletContext, HealthCheckRegistry healthCheckRegistry) {
        healthCheckRegistry.register("Launch datasource", new DataSourceHealthCheck(JeeResourceLocator.DEFAULT_DATASOURCE_NAME));
        healthCheckRegistry.register("Redis", ConfigurationEngine.getInstance(RedisHealthCheck.class));
        healthCheckRegistry.register("Membership subscription message publisher", new KafkaTopicHealthCheck(MEMBERSHIP_SUBSCRIPTION_TOPIC));
        healthCheckRegistry.register("Membership update message publisher", new KafkaTopicHealthCheck(MEMBERSHIP_UPDATE_TOPIC));
        healthCheckRegistry.register("Welcome email message publisher", new KafkaTopicHealthCheck(WELCOME_EMAIL_TOPIC));
        servletContext.setAttribute(HealthCheckServlet.REGISTRY_KEY, healthCheckRegistry);
    }

    private void includeConfigurationsInDump() {
        REFLECTIONS.getTypesAnnotatedWith(ConfiguredInPropertiesFile.class)
                .forEach(properties -> DUMP_STATE_REGISTRY.add(new ConfiguredBeanDumpState(properties)));
        Arrays.asList(EXT_SERVICES)
                .forEach(service -> DUMP_STATE_REGISTRY.add(new ServiceDumpState(service)));
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        event.getServletContext().log("context destroyed");
    }

    private void initConfigurationEngine(ServletContext servletContext, Module... restUtilsModules) {
        ConfigurationEngine.init(restUtilsModules);
        servletContext.log(CONFIGURATION_ENGINE);
    }

    private void initLog4J(ServletContext servletContext) {
        VirtualFile virtualFile = VFS.getChild(new ConfigurationFilesManager().getConfigurationFileUrl(LOG4J_FILENAME, this.getClass()).getFile());
        URL fileRealURL;
        try {
            fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
            servletContext.log("Log4j has been initialized with config file " + fileRealURL.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");
        } catch (IOException e) {
            servletContext.log("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be load", e);
        }
    }

    private TrackingClientConfiguration createTrackingConfiguration() {
        return new TrackingClientConfiguration
                .Builder(APPLICATION_NAME)
                .build();
    }

    private void warmUp(ServletContext servletContext) {
        servletContext.log("Performing warmUp");
        ConfigurationEngine.getInstance(MembershipControllersWarmUp.class).warmUp();
    }
}
