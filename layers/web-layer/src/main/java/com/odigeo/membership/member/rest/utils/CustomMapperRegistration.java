package com.odigeo.membership.member.rest.utils;

import com.odigeo.bookingapi.v12.responses.CreditCard;
import com.odigeo.bookingapi.v12.responses.CreditCardType;
import com.odigeo.membership.MemberSubscriptionDetails;
import ma.glasnost.orika.MapperFactory;

public final class CustomMapperRegistration {

    private static final String SUBSCRIPTION_PAYMENT_METHOD_TYPE = "subscriptionPaymentMethodType";

    private CustomMapperRegistration() {
    }

    public static void registerCreditCardTypeMapper(MapperFactory mapperFactory) {
        mapperFactory.classMap(CreditCardType.class, com.odigeo.membership.response.CreditCardType.class)
                .field("code", "creditCardCode")
                .field("name", "creditCardName")
                .byDefault().register();
    }

    public static void registerMemberSubscriptionDetailsMapper(MapperFactory mapperFactory) {
        mapperFactory.classMap(MemberSubscriptionDetails.class, com.odigeo.membership.response.MemberSubscriptionDetails.class)
                .field(SUBSCRIPTION_PAYMENT_METHOD_TYPE, SUBSCRIPTION_PAYMENT_METHOD_TYPE)
                .field(SUBSCRIPTION_PAYMENT_METHOD_TYPE, "subscriptionPaymentMethod.creditCardType.creditCardType")
                .byDefault().register();
    }

    public static void registerCreditCardMapper(MapperFactory mapperFactory) {
        mapperFactory.classMap(CreditCard.class, com.odigeo.membership.response.CreditCard.class)
                .field("expirationMonth", "expirationDateMonth")
                .field("expirationYear", "expirationDateYear")
                .field("expirationDate", "expirationDateYearFormated")
                .byDefault().register();
    }
}
