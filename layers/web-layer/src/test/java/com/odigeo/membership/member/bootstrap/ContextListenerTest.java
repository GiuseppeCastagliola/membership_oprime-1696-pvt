package com.odigeo.membership.member.bootstrap;

import com.google.inject.Module;
import com.odigeo.commons.rest.monitoring.healthcheck.AutoHealthCheckRegister;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class ContextListenerTest {

    private static final int EXPECTED_MODULES_NUMBER = 12;
    private ContextListener contextListener;
    private ServletContextEvent servletContextEventMock;

    @BeforeMethod
    public void setUp() {
        setUpMocks();
    }

    private void setUpMocks() {
        this.contextListener = new ContextListener();
        this.servletContextEventMock = mock(ServletContextEvent.class);
        ServletContext servletContextMock = mock(ServletContext.class);
        when(servletContextEventMock.getServletContext()).thenReturn(servletContextMock);
    }

    @Test
    public void testContextDestroyed() {
        try {
            this.contextListener.contextDestroyed(servletContextEventMock);
        } catch (final Exception e) {
            fail();
        }
    }

    @Test
    public void testAssembleModulesList() {
        final List<Module> receivedList = contextListener.assembleModulesList(mock(AutoHealthCheckRegister.class));
        assertEquals(receivedList.size(), EXPECTED_MODULES_NUMBER);
    }
}
