package com.odigeo.membership.member.rest.exception.mapper;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.validation.ValidationException;
import javax.ws.rs.core.Response;

public class ValidationExceptionMapperTest {

    @Test
    public void testValidationExceptionMapper() {
        ValidationExceptionMapper mapper = new ValidationExceptionMapper();
        ValidationException exception = new ValidationException("TEST");
        Response response = mapper.toResponse(exception);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }
}
