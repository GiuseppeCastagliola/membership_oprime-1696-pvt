package com.odigeo.membership.member.rest.utils;

import org.testng.annotations.Test;

import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ResponseBuilderTest {
    @Test
    public void buildResponseEntityTest() {
        Response response = ResponseBuilder.buildResponse(null, null);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
        response = ResponseBuilder.buildResponse(new Entity(0), null);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
        Request request = mock(Request.class);
        when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(null);
        response = ResponseBuilder.buildResponse(new Entity(1), request);
        assertEquals(response.getStatus(), Response.Status.OK.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
        when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(Response.status(Response.Status.NOT_MODIFIED));
        when(request.getMethod()).thenReturn("GET");
        response = ResponseBuilder.buildResponse(new Entity(1), request);
        assertEquals(response.getStatus(), Response.Status.NOT_MODIFIED.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
    }

    private static final class Entity {
        private int intField;

        private Entity(final int intField) {
            this.intField = intField;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Entity)) {
                return false;
            }
            final Entity entity = (Entity) o;
            return intField == entity.intField;
        }

        @Override
        public int hashCode() {
            return intField;
        }
    }
}